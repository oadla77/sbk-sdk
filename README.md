# sbk.io SDK

## Installing the library from the private repository

bower install sbk-sdk --save

## Getting Started

Include the library in your index.html

    <script src="bower_components/dist/sdk.js"></script>

Instantiate a new engine: 

    var platform = new Sbk();
    
By default, the engine will point to https://api.sbk.io. If you want to work with a local development engine, just use the url option:

    var platform = new Sbk({url: 'http://localhost:5000'});
    
sbk.io is using JWT tokens for authentication. In order to use most of the available methods and helpers, you need to retrieve a valid token.

### How to instantiate in Node:

    var Sbk = require('@sbk/sbk-sdk');
    var SbkSDK = new Sbk();
    SbkSDK.devices.list().then(function(devices){
        console.log(devices);
        return devices
    });

### Configuring our SDK to retrieve a token

You need to set the following options in order to retrieve a valid token:

    url: Url of the sbk.io instance you want to use. default to https://api.sbk.io
    auth: {
        token: An existing token (previously retrieved) or nothing
    },
    unsafe: true indicates that we're in an unsafe environment (client app, browser, etc)
    app_key : The key of the app as provided by sbk.io
    app_secret: Optional. The secret if you are in a safe environment (do not use for client or browser app)

With these options you can instantiate the Sbk instance:

    var platform = new Sbk({
        unsafe: isClient,
        app_key: "b2423a98430c03213d7ad86033622f5066a44b69"
    });

Note that we haven't provided any token as we don't have one yet.

### Retrieving a valid token

You execute the authenticate method to retrieve your token :

    platform.authenticate({username:username, password:password}).then(function(result) {

        // Save the token for subsequent calls
        platform.auth = {
            token: result.token
        };

    });

