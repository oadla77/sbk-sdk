declare class Sbk extends NodeJS.EventEmitter {
    constructor(options: Sbk.Options);

    base_url: string;
    apiVersion: string;

    authenticate(credentials: Sbk.Credentials): Promise<Sbk.Authentication.UserResult | undefined>;
    register(user: Sbk.Authentication.Register, options?: Sbk.Http.Options): Promise<Sbk.User | undefined>;

    auth: Sbk.Auth;
    http: Sbk.Http.Client;
    logger: Sbk.Logger;

    calls: Sbk.Streams;
    events: Sbk.Events;
    notifications: Sbk.Notifications;
    permissions: Sbk.Permissions;
    translations: Sbk.Translations;
    user: Sbk.Users;
    templates: Sbk.Templates;
    system: Sbk.System;
    authentication: Sbk.Authentication;
    groups: Sbk.Groups;
    feature: Sbk.Features;
}

declare namespace Sbk {
    interface Options {
        url?: string;
        apiVersion?: string;
        api_version?: string;
        auth?: any;
        log_level?: number;
        unsafe: boolean;
        app_key: string;
    }

    interface Auth {
        token?: string;
    }

    interface Credentials {
        username: string;
        password: string;
    }

    interface User {
        _id: string;
        username: string;
        firstname?: string;
        lastname?: string;
        company?: string;
        timezone?: string;
        email: string;
        phone?: string;
        birthday?: string;
        createdTs?: string;
        last_access_ts?: string;
        medias?: any;
        mailingAddress?: any;
        billingAddress?: any;
        billingId?: string;
        comments?: string;
        validation_code?: string;
        resetPasswordToken?: string;
        resetPasswordExpires?: string;
        resetPasswordExpiredToken?: string[];
        agree: boolean;
        status: UserStatus;
        background?: any;
        profileType?: UserProfileType;
        profiles: UserProfile[];
        referrer?: string;
        notificationConfig: {
            email?: string;
            phone?: string;
        };
        localeId: string;
        schemaVersion?: number;
        tokenVersion?: number;
        security?: {
            question?: string;
            answer?: string;
        };
    }

    interface UserProfile {
        type: string;
        app?: string;
        showAlarm?: boolean;
        preferences?: any;
    }

    type UserStatus =
        | 'NEW'
        | 'PENDING_EMAIL_VERIFICATION'
        | 'PENDING_APPROVAL'
        | 'PENDING_FIRST_LOGIN'
        | 'ACTIVE'
        | 'SUSPENDED'
        | 'REVOKED';

    type UserProfileType =
        | 'user'
        | 'noc_owner'
        | 'noc_admin'
        | 'noc_installer'
        | 'noc_agent';

    interface Service {
        _id: string;
        name: string;
        apiKey: string;
        status: ServiceStatus;
        type?: string;
        group?: string;
        isTrusted: boolean;
        createdBy: {who: string; whoType: string};
        createdOn: string;
        updatedOn: string;
    }

    type ServiceStatus =
        | 'ACTIVE'
        | 'REMOVED'
        | 'PENDING_APPROVAL'
        | 'SUSPENDED';

    interface Logger {
        level: number;
        trace(...args: any[]): void;
        debug(...args: any[]): void;
        info(...args: any[]): void;
        warn(...args: any[]): void;
        error(...args: any[]): void;
        fatal(...args: any[]): void;
    }

    interface Device {
        _id: string;
        name?: string;
        icon?: string;
        parent?: string;
        gateway?: string;
        description?: string;
        isEmulated?: boolean;
        isHidden?: boolean;
        showWarning?: boolean;
        status: DeviceStatus;
        statusUpdatedAt?: string;
        mode?: DeviceMode;
        sysHealth?: any;
        config?: any;
        protocol?: string;
        protocolData?: any;
        model: string;
        location: string;
        createdBy?: string;
        parameters?: any;
        userParams?: any;
        properties?: any;
        createdTs: string;
        lastPing?: string;
        latency?: number;
        publicKey?: string;
    }

    type DeviceStatus =
        | 'NEW'
        | 'ACTIVE'
        | 'SUSPENDED'
        | 'REVOKED'
        | 'FAILED'
        | 'REMOVED';

    type DeviceMode =
        | 'LIVE'
        | 'TEST'
        | 'DISABLED';

    interface Model {
        _id: string;
        name?: string;
        readableId?: string;
        manufacturer?: string;
        productCode?: string;
        protocolId?: string;
        type?: string;
        description?: string;
        gui?: any;
        nocModelTypes?: any;
        capabilities?: Capability.Model[];
        parameters?: DeviceParameterModel[];
        protocolName?: string;
        protocolInfo?: any;
        label?: any;
        usesStreams?: boolean;
        createdTs: string;
        publish?: boolean;
        hideByDefault?: boolean;
    }

    interface Capability {
        name?: string;
        timestamp?: string;
        value: Capability.Value;
        unit?: string;
    }

    namespace Capability {
        type EnumValue = string;
        type ScalarValue = number;
        type ColorValue = {[channel: string]: number};
        type FlagsValue = {[flag: string]: boolean};
        type UriValue = string;
        type Value = EnumValue | ScalarValue | ColorValue | FlagsValue | UriValue;

        type Method = 'get' | 'put';

        type EnumRange = string[];
        type ScalarRange = {value: ScalarValue; unit: string}[];
        type ColorRange = string[];
        type FlagsRange = string[];
        type Range = EnumRange | ScalarRange | ColorRange | FlagsRange;

        interface Model {
            capabilityId: string;
            description?: string;
            type: Type;
            visible?: boolean;
            range?: Range;
            precision?: number;
            units?: string[];
            methods: Method[];
            example?: string;
        }

        type Type =
            | 'enum'
            | 'float'
            | 'color'
            | 'flags'
            | 'uri';
    }

    interface DeviceParameterModel {
        parameter: string;
        default: string;
        range: any[];
        type: string;
    }

    interface Event<T> {
        type: EventType;
        path: string;
        who: string;
        whoType: string;
        gateway: string;
        timestamp: string;
        resource: Partial<T>;
    }

    type EventType =
        | 'create'
        | 'update'
        | 'delete';

    interface Location {
        _id: string;
        name: string;
        icon?: string;
        type: string;
        description?: string;
        status: LocationStatus;
        parent?: string;
        createdBy?: string;
        createdTs: string;
        address?: any;
        trackGeo?: boolean;
        geo?: number[];
        medias?: any;
        contact?: any;
        children?: Location[];
    }

    type LocationStatus =
        | 'ACTIVE'
        | 'REMOVED';

    interface Feature {
        _id: string;
        name: string;
        status: FeatureStatus;
        default: boolean;
        validity: {
            from: string;
            to: string;
        };
        rlock: Feature.RegionLock;
        regions: string[];
        createdOn: string;
        updatedOn: string;
    }

    type FeatureStatus =
        | 'ACTIVE'
        | 'DEPRECATED'
        | 'DISABLED'
        | 'ENABLED'
        | 'OPTIONAL'
        | 'REMOVED';

    namespace Feature {
        type RegionLock =
            | 'NONE'
            | 'WHITELIST'
            | 'BLACKLIST';
    }

    interface FeatureFlag {
        user: string;
        group: string;
        feature: string;
        status: FeatureFlagStatus;
    }

    type FeatureFlagStatus =
        | 'ENABLED'
        | 'DISABLED';

    interface FeatureGroup {
        _id: string;
        name: string;
        type: FeatureGroupType;
        members: string[];
        service: string;
        createdOn: string;
        updatedOn: string;
    }

    type FeatureGroupType =
        | 'MEMBERSHIP'
        | 'COHORT'
        | 'OPTIONAL'
        | 'SUBSCRIPTION';

    interface Group {
        _id: string;
        name?: string;
        type: string;
        status: string;
        members: GroupMember[];
        children: GroupChild[];
        createdOn: string;
        updatedOn: string;
    }

    interface GroupMember {
        who: string;
        whoType: string;
    }

    interface GroupChild {
        _id: string;
        rights: {
            range: { to: string; from: string; };
            manage: boolean;
            share: boolean;
            write: boolean;
            read: boolean;
        }
    }

    interface Permission {
        _id: string;

        resource: string;
        type: string;
        who: string;
        whoType: string;

        write: boolean;
        read: boolean;
        manage: boolean;
        share: boolean;

        range: { from: string; to: string; };

        status: string;
        creationDate: string;
    }

    interface Notification {
        _id: string;
        message: any;
        type: string;
        createdTs: string;
        resources: any;
        read: boolean;
        readTs: string;
        status: string;
    }

    interface NotificationConfig {
        _id: string;
        home: string;
        user: string;
        config: any;
        createdTs: string;
    }

    interface SmsEmail {
        location: string;
        user: string;
        notification: SmsEmailPreferences;
    }

    interface SmsEmailPreferences {
        normal: SmsEmailPreference;
        warn: SmsEmailPreference;
        alert: SmsEmailPreference;
    }

    interface SmsEmailPreference {
        sms: boolean;
        email: boolean;
    }

    interface Scenario {
        _id: string;
        name?: string;
        gateway: string;
        trigger: Scenario.Trigger;
        action: Scenario.Action;
        enabled: boolean;
        schedule?: string;
        cooldown?: number;
        createdTs: string;
        triggeredAt?: string;
        status: string;
    }

    namespace Scenario {
        type Trigger =
            | CapabilityTrigger
            | BootTrigger
            | ScheduleTrigger;

        interface CapabilityTrigger {
            type: 'capability';
            device: SelectCondition;
            capability: SelectCondition;
            condition: Condition;
        }

        interface BootTrigger {
            type: 'startup' | 'shutdown';
        }

        interface ScheduleTrigger {
            type: 'schedule';
            schedule: string;
        }

        type Action =
            | SetEnumCapabilityAction
            | SetScalarCapabilityAction
            | SequenceAction;

        interface SetEnumCapabilityAction {
            type: 'setCapability';
            device: string;
            capability: string;
            value: string;
        }

        interface SetScalarCapabilityAction {
            type: 'setCapability';
            device: string;
            capability: string;
            value: number;
            unit: string;
        }

        interface SequenceAction {
            type: 'sequence';
            actions: Action[];
        }
    }

    interface Schedule {
        _id: string;
        name: string;
        schedules: string;
        timezone: string;
        frequency: string;
        occurenceType: string;
        status: string;
        createdTs: string;
        gateway: string;
    }

    interface Supervision {
        _id: string;
        config: Supervision.Config;
        status: SupervisionStatus;
        statusAt: string;
        suspendPeriod: number;
        activeTrigger: Supervision.ActiveTrigger | null;
        triggers: Supervision.Trigger[];
        reports: Supervision.Report[];
    }

    type SupervisionStatus =
        | 'ACTIVE'
        | 'INACTIVE'
        | 'TRIGGERED'
        | 'SUSPENDED'
        | 'DELAY'
        | 'TRIGGER_DELAY'
        | 'REMOVED';

    namespace Supervision {
        interface Config {
            name: string;
            permanent: boolean;
            gateway: string;
            display: DisplayConfig;
            inactiveDisplay: DisplayConfig;
            triggers: TriggerConfig[];
            triggered: TriggeredConfig;
            activation: ActivationConfig;
            deactivation: DeactivationConfig;
        }

        interface DisplayConfig {
            title: string;
            description: string;
        }

        interface TriggerConfig {
            device: string;
            capability: string;
            condition: Condition;
            delay: number;
        }

        interface TriggeredConfig {
            type: NotificationType;
            notification: boolean;
            actions: ActionConfig[];
            confirmationPeriod: number;
            service: string;
        }

        type NotificationType =
            | 'ORANGE'
            | 'RED';

        interface ActivationConfig {
            delay: number;
            autoBypass: boolean;
            pinRequired: boolean;
            notification: boolean;
            actions: ActionConfig[];
        }

        interface DeactivationConfig {
            suspendPeriod?: number;
            pinRequired: boolean;
            notification: boolean;
            actions: ActionConfig[];
        }

        type ActionConfig = EnumActionConfig | ScalarActionConfig;

        interface EnumActionConfig {
            device: string;
            capability: string;
            value: string;
        }

        interface ScalarActionConfig {
            device: string;
            capability: string;
            value: number;
            unit: string;
        }

        interface ActiveTrigger {
            device: string;
            capability: string;
            triggeredAt: string;
            delay: number;
        }

        interface Trigger {
            device: string;
            capability: string;
            status: TriggerStatus;
            statusAt: string;
        }

        type TriggerStatus =
            | 'READY'
            | 'DELAY'
            | 'TRIGGERED'
            | 'BYPASSED';

        interface Report {
            who: string;
            whoType: string;
            situation: ReportSituation;
            timestamp: string;
        }

        type ReportSituation =
            | 'ALL_CLEAR'
            | 'UNKNOWN'
            | 'ALARM';
    }

    interface Stream {
        _id: string;
        device: string;
        quality: StreamQuality;
        streaming: boolean;
        recording: boolean;
        endpoint: string;
        RTMPendpoint: string;
        HLSendpoint: string;
        created: string;
    }

    type StreamQuality = 'LOW' | 'HIGH';

    interface StreamRecord {
        _id: string;
        stream: string;
        recording: boolean;
        start: string;
        stop: string;
        file: string;
    }

    type Condition =
        | Condition.Always
        | Condition.Never
        | Condition.Exact
        | Condition.Comparison
        | Condition.NotCondition
        | Condition.AllCondition
        | Condition.AnyCondition;

    type SelectCondition =
        | Condition.Always
        | Condition.Never
        | Condition.Exact
        | Condition.NotSelectCondition
        | Condition.AnySelectCondition;

    namespace Condition {
        interface Always {
            type: 'always';
        }
        interface Never {
            type: 'never';
        }
        interface Exact {
            type: 'exact';
            value: string;
        }
        interface Comparison {
            type: '<' | '<=' | '>' | '>=' | '=' | '!=';
            value: number;
            unit: string;
            epsilon?: number;
        }
        interface Not<T> {
            type: 'not';
            condition: T;
        }
        interface All<T> {
            type: 'all';
            conditions: T[];
        }
        interface Any<T> {
            type: 'any';
            conditions: T[];
        }

        // Type aliases cannot reference themselves as generic type parameters,
        // so we special case on the types we need here.
        //
        // If this issue is fixed, the Condition/SelectCondition types should
        // be definable like
        //
        //     ... | Condition.Exact | Condition.Not<Condition> | ...
        //
        // without requiring the following interface definitions.
        //
        // See https://github.com/Microsoft/TypeScript/issues/6230
        interface NotCondition extends Not<Condition> {}
        interface AllCondition extends All<Condition> {}
        interface AnyCondition extends Any<Condition> {}
        interface NotSelectCondition extends Not<SelectCondition> {}
        interface AnySelectCondition extends Any<SelectCondition> {}
    }

    interface Template {
        _id: string;
        name: string;
        type: string;
        status: string;
        resources: TemplateResource[];
        createdAt: string;
        updatedAt: string;
    }

    interface TemplateResource {
        _id: string;
        type: string;
    }

    interface Translation {
        key: string;
        en?: string;
        fr?: string;
    }

    type Result<T> = Promise<Http.Response<T>>;
    type Maybe<T> = Promise<Http.ResponseMaybe<T>>;

    interface Authentication {
        platform: Sbk;
        register(user: Authentication.Register): Result<Authentication.UserResult>;
        gatewayNonce(gatewayId: string): Result<GatewayNonce>;
        forgot(email: string): Result<void>;
        resendValidateEmail(email: string): Result<void>;
        reset(resetToken: string): Result<Authentication.ResetToken>;
        resetPassword(resetToken: string, passwordInfo: Authentication.NewPassword): Result<User>;
        confirm(key: string): Result<User>;
        verify(token: string): Promise<Authentication.SubscribeKey>;
        renew(): Promise<Authentication.Token & Authentication.SubscribeKey>;
        authenticateService(name: string, key: string): Result<Authentication.ServiceResult>;
    }

    interface GatewayNonce {
        nonce: string;
        servername: string;
        cert: string;
        url: string;
    }

    namespace Authentication {
        interface Register {
            password: string;
            pass_repeat: string;
            firstname: string;
            lastname: string;
            company?: {
                name: string;
                phone: string;
                email: string;
                address: string;
                position: string;
            };
            timezone?: string;
            email: string;
            phone: string;
            media?: any;
            mailingAddress?: any;
            billingAddress?: any;
            billingId?: string;
            comments?: string;
            agree: boolean;
            status?: string;
            homes?: string;
            profileType?: UserProfileType;
            localeId?: string;
            background?: any;
            referrer?: string;
        }

        interface ResetToken {
            resetToken: string;
        }

        interface NewPassword {
            newPassword: string;
            newPasswordConfirm: string;
        }

        interface SubscribeKey {
            subscribeKey: string;
        }

        interface Token {
            token: string;
        }

        type UserResult = User & Token & SubscribeKey
        type ServiceResult = Service & Token
    }

    interface Devices {
        platform: Sbk;
        list(query?: any): Result<Device[]>;
        get(deviceId: string, query?: any): Result<Device>;
        getChildren(deviceId: string, level?: number, query?: any): Result<Device[]>;
        create(device: Devices.Create, query?: any): Result<Device>;
        update(device: Devices.Update, query?: any): Result<Device>;
        patch(device: Devices.Update, query?: any): Result<Device>;
        delete(deviceId: string, query?: any): Result<void>;
        reset(gatewayId: string): Result<void>;
        updateCapability(deviceId: string, capabilityId: string, value: Capability.Value, unit?: string, timestamp?: string): Maybe<Capability>;
        patchCapability(deviceId: string, capabilityId: string, cap: Capability): Maybe<Capability>;
        getCapability(deviceId: string, capabilityId: string): Result<Capability>;
        listCapability(deviceId: string): Result<Capability[]>;
        getEvents(gatewayId: string): Result<Event<any>[]>;
    }

    namespace Devices {
        interface Create {
            model: Model | string;
            location?: Location | string;
            parent?: string;
            gateway?: string;
            name: string;
            mac?: string;
            isEmulated?: boolean;
            showWarning?: boolean;
            description?: string;
            icon?: string;
            protocol?: string;
            parameters?: any;
            properties?: any;
            onBehalfOfUser?: string;
            remoteAccess?: any;
            userParams?: any;
        }

        interface Update {
            _id: string;
            model?: Model | string;
            location?: Location | string;
            parent?: string;
            gateway?: string;
            name?: string;
            description?: string;
            isEmulated?: boolean;
            showWarning?: boolean;
            config?: any;
            protocol?: string;
            mode?: string;
            parameters?: any;
            properties?: any;
            userParams?: any;
        }
    }

    interface Events {
        platform: Sbk;
        create<T>(event: Event<T>): Result<void>;
        create<T>(path: string, type: EventType, resource?: Partial<T>, timestamp?: string, requestId?: string): Result<void>;
    }

    interface Locations {
        platform: Sbk;
        list(level?: number): Result<Location[]>;
        get(locationId: string, level?: number): Result<Location>;
        getRoot(locationId: string, level?: number): Result<Location>;
        create(location: Locations.Create): Result<Location>;
        update(location: Locations.Update): Result<Location>;
        patch(location: Locations.Patch, locationId: string): Result<Location>;
        delete(locationId: string): Result<void>;
    }

    namespace Locations {
        interface Create {
            name: string;
            address?: {
                address?: string;
                suite?: string;
                city?: string;
                state?: string;
                zip?: string;
                country?: string;
            };
            parent?: string;
            icon?: string;
            description?: string;
            trackGeo?: boolean;
            medias?: any[];
            contact?: {
                phone?: string;
                label?: string;
            }
        }

        interface Update extends Patch {
            _id: string;
        }

        interface Patch {
            name: string;
            address?: {
                address?: string;
                suite?: string;
                city?: string;
                state?: string;
                zip?: string;
                country?: string;
            };
            parent?: string;
            icon?: string;
            description?: string;
            trackGeo?: boolean;
            medias?: any[];
        }
    }

    interface Models {
        platform: Sbk;
        list(query?: Models.Query): Result<Model[]>;
        get(id: string): Result<Model>;
        search(query: Models.Query): Result<Model[]>;
    }

    namespace Models {
        interface Query {
            manufacturer?: string;
            model?: string;
            type?: string;
            key?: string;
            _id?: string;
            protocolname?: string;
        }
    }

    interface Features {
        platform: Sbk;
        list(query?: Features.Query): Result<Feature[]>;
        get(featureId: string): Result<Feature>;
        create(feature: Features.Create): Result<Feature>;
        update(featureId: string, feature: Features.Update): Result<Feature>;
        listGroups(query: Features.GroupQuery): Result<FeatureGroup[]>;
        getGroup(groupId: string): Result<FeatureGroup>;
        createGroup(group: Features.CreateGroup): Result<FeatureGroup>;
        addMemberToGroup(groupId: string, uid: string): Result<FeatureGroup>;
        removeMemberFromGroup(groupId: string, uid: string): Result<FeatureGroup>;
        associateGroup(featureId: string, groupId: string, data: Features.AssociateFlag): Result<FeatureFlag>;
        dissociateGroup(featureId: string, groupId: string): Result<void>;
        associateUser(featureId: string, uid: string, data: Features.AssociateFlag): Result<FeatureFlag>;
        dissociateUser(featureId: string, uid: string): Result<void>;
        getFlags(query?: Features.FlagsQuery): Result<string[]>;
    }

    namespace Features {
        interface Query {
            name?: string;
            status?: string;
        }

        interface Create {
            _id: string;
            name: string;
            default?: boolean;
            validity?: {
                from?: string;
                to?: string;
            };
            rlock?: Feature.RegionLock;
            regions?: string[];
        }

        interface Update {
            name?: string;
            default?: boolean;
            validity?: {
                from?: string;
                to?: string;
            };
            rlock?: Feature.RegionLock;
            regions?: string[];
        }

        interface GroupQuery {
            populate: string;
        }

        interface CreateGroup {
            name: string;
            type: FeatureGroupType;
            members?: string[];
        }

        interface AssociateFlag {
            enable?: boolean;
        }

        interface FlagsQuery {
            uid?: string;
            region?: string;
        }
    }

    interface Groups {
        platform: Sbk;
        list(): Result<Group[]>;
    }

    interface Permissions {
        platform: Sbk;
        list(userId: string): Result<Permission[]>;
    }

    interface Notifications {
        platform: Sbk;
        list(query?: Notifications.Query): Result<Notification[] | Notifications.Count>;
        get(locationId: string, query?: Notifications.Query): Result<Notification[] | Notifications.Count>;
        patch(notificationId: string, patch: Notifications.Patch): Result<Notification>;
        markRead(notificationId: string): Result<Notification>;
        getConfig(): Result<NotificationConfig>;
        updateConfig(update: Notifications.UpdateConfig): Result<NotificationConfig>;
    }

    namespace Notifications {
        interface Query {
            count?: boolean;
            read?: boolean;
            search?: string;
            date_from?: number;
            date_to?: number;
            skip?: number;
            limit?: number;
        }

        interface Count {
            count: number;
        }

        interface Patch {
            read?: boolean;
            readTs?: string;
        }

        interface UpdateConfig {
            _id?: string;
            config: any;
        }
    }

    interface Scenarios {
        platform: Sbk;
        list(): Result<Scenario[]>;
        get(scenarioId: string): Result<Scenario>;
        create(scenario: Scenarios.Create): Result<Scenario>;
        update(scenario: Scenarios.Update): Result<Scenario>;
        delete(scenarioId: string): Result<void>;
    }

    namespace Scenarios {
        interface Create {
            enabled?: boolean;
            name?: string;
            trigger: Scenario.Trigger;
            action: Scenario.Action;
            schedule?: string;
            gateway?: string;
            cooldown?: number;
        }

        interface Update extends Create {
            _id: string;
        }
    }

    interface Schedules {
        platform: Sbk;
        list(): Result<Schedule[]>;
        get(scheduleId: string): Result<Schedule>;
        create(schedule: Schedules.Create): Result<Schedule>;
        update(schedule: Schedules.Update): Result<Schedule>;
        delete(scheduleId: string): Result<void>;
    }

    namespace Schedules {
        interface Create {
            name: string;
            schedules: string;
            timezone: string;
            occurenceType: string;
            frequency: string;
            gateway: string;
        }

        interface Update extends Create {
            _id: string;
        }
    }

    interface Supervisions {
        platform: Sbk;
        list(): Result<Supervision[]>;
        get(supervisionId: string): Result<Supervision>;
        create(supervision: Supervisions.Config): Result<Supervision>;
        update(supervisionId: string, supervision: Supervisions.Config): Result<Supervision>;
        delete(supervisionId: string): Result<void>;
        activate(supervisionId: string, action: Supervisions.Activate): Result<Supervision>;
    }

    namespace Supervisions {
        interface Config {
            name?: string;
            permanent?: string;
            gateway: string;
            display?: Display;
            inactiveDisplay?: Display;
            triggers?: Trigger[];
            activation?: {
                delay?: number;
                autoBypass?: boolean;
                pinRequired?: boolean;
                notification?: boolean;
                actions?: Action[];
            };
            deactivation?: {
                suspendPeriod?: number;
                pinRequired?: boolean;
                notification?: boolean;
                actions?: Action[];
            };
            triggered?: {
                type?: Supervision.NotificationType;
                notification?: boolean;
                actions?: Action[];
                confirmationPeriod?: number;
                service?: string | null;
            };
        }

        interface Display {
            title?: string;
            description?: string;
            status?: string;
        }

        interface Trigger {
            device: string;
            capability: string;
            condition: Condition;
            delay?: number;
        }

        type Action = EnumAction | ScalarAction;

        interface EnumAction {
            device: string;
            capability: string;
            value: string;
        }

        interface ScalarAction {
            device: string;
            capability: string;
            value: number;
            unit: string;
        }

        interface Activate {
            action: ActivateAction;
            pinToken?: string;
            situation?: Supervision.ReportSituation;
            suspendPeriod?: number;
            bypasses?: ActivateBypass[];
        }

        type ActivateAction =
            | 'ACTIVATE'
            | 'DEACTIVATE'
            | 'SUSPEND'
            | 'REPORT';

        interface ActivateBypass {
            device: string;
            capability: string;
        }
    }

    interface Streams {
        platform: Sbk;
        list(): Result<Stream[]>;
        listRecord(device: string): Result<StreamRecord[]>;
        startRecord(device: string, quality: StreamQuality): Result<StreamRecord>;
        stopRecord(device: string, quality: StreamQuality): Result<StreamRecord>;
    }

    interface System {
        platform: Sbk;
        info(): Result<System.Info>;
    }

    namespace System {
        interface Info {
            short: string;
            long: string;
            branch: string;
            tag: string;
            db: string;
            version: string;
            channel: string;
            app_env: string;
            host: string;
            request: string;
            nodeVersion: string;
        }
    }

    interface Templates {
        platform: Sbk;
        list(): Result<Template[]>;
        get(templateId: string): Result<Template>;
        create(template: Templates.Create): Result<Template>;
        patch(template: Templates.Patch): Result<Template>;
        delete(templateId: string): Result<Template>;
    }

    namespace Templates {
        interface Create {
            name: string;
            type: string;
            status?: string;
            resources?: TemplateResource[];
        }

        interface Patch {
            name?: string;
            resources?: TemplateResource[];
        }
    }

    interface Translations {
        platform: Sbk;
        list(): Result<Translation[]>;
        get(key: string): Result<Translation>;
        create(translation: Translation): Result<Translation>;
        update(key: string, translation: Translation): Result<Translation>;
    }

    interface Users {
        platform: Sbk;
        get(username: string): Result<User>;
        register(user: Authentication.Register): Result<User>;
        forgot(email: string): Result<void>;
        resendValidateEmail(email: string): Result<void>;
        reset(resetToken: string): Result<Authentication.ResetToken>;
        resetPassword(resetToken: string, passwordInfo: Authentication.NewPassword): Result<User>;
        confirm(key: string): Result<User>;
        changePassword(username: string, currentPassword: string, password: string): Result<User>;
        list(): Result<User[]>;
        getPermissions(userId: string, query?: Users.PermissionsQuery): Result<Permission[]>;
        getCurrentUser(options?: Users.CurrentUserOptions): Result<User>;
        update(username: string, user: Users.Update): Result<User>;
        patch(username: string, user: Users.Patch): Result<User>;
        updateProfile(username: string, type: string, profile: Users.Profile): Result<User>;
        enable(username: string): Result<Users.EnableResult>;
        delete(username: string): Result<Users.EnableResult>;
        bindDevice(code: string, location: string, name: string): Result<Device>;
        listSmsEmailPreferences(): Result<SmsEmail[]>;
        updateSmsEmailPreference(locationId: string, preference: Users.SmsEmailPreference): Result<SmsEmail>;
        updatePin(data: Users.UpdatePin): Result<void>;
        verifyPin(data: Users.VerifyPin): Result<Users.PinToken>;
    }

    namespace Users {
        interface PermissionsQuery {
            noc?: string;
            type?: string;
        }

        interface CurrentUserOptions {
            token?: string;
            security?: boolean;
        }

        interface Update {
            firstname?: string;
            lastname?: string;
            company?: string;
            timezone?: string;
            email?: string;
            medias?: any;
            comments?: any;
            agree?: boolean;
            homes?: any[];
            localeId?: string;
            background?: any;
            notificationConfig?: { phone?: string; email?: string; };
        }

        interface Patch extends Update {
            referrer?: string;
        }

        interface Profile {
            showAlarm?: boolean;
            preferences?: any;
        }

        interface EnableResult {
            message: string;
        }

        interface SmsEmailPreferences {
            normal: SmsEmailPreference;
            warn: SmsEmailPreference;
            alert: SmsEmailPreference;
        }

        interface SmsEmailPreference {
            sms?: boolean;
            email?: boolean;
        }

        interface UpdatePin {
            password: string;
            pin: string;
            security?: {
                question: string;
                answer: string;
            };
        }

        interface VerifyPin {
            pin: string;
            gateway: string;
        }

        interface PinToken {
            token: string;
        }
    }

    interface ZWave {
        platform: Sbk;
        exclusion(gatewayId: string): Result<void>;
        info(deviceId: string): Result<ZWave.Info>;
        cancel(gatewayId: string): Result<void>;
        interview(deviceId: string): Result<void>;
        getDsk(gatewayId: string): Result<ZWave.DSKRequest>;
        getKeyRequest(gatewayId: string): Result<ZWave.KeyRequest>;
        postDsk(gatewayId: string, req: ZWave.DSKRequest): Result<void>;
        postKeyRequest(gatewayId: string, req: ZWave.KeyRequest): Result<void>;
        learn(gatewayId: string, req?: ZWave.LearnRequest): Result<void>;
        replace(deviceId: string): Result<void>;
        update(gatewayId: string): Result<void>;
        reset(gatewayId: string): Result<void>;
        sendNif(gatewayId: string): Result<void>;
    }

    namespace ZWave {
        interface DSKRequest {
            pinRequired: boolean;
            dsk: string;
        }

        interface KeyRequest {
            keys: {
                S2Unauthenticated: boolean;
                S2Authenticated: boolean;
                S2AccessControl: boolean;
                S0Unauthenticated: boolean;
            };
            clientSideAuth?: boolean;
            pin?: string;
        }

        interface LearnRequest {
            classic: boolean;
        }

        interface Info {
            node_id: number;
            endpoint_id: number;
            node_properties: string[];
            vendor_id: number;
            product_type: number;
            product_id: number;
            dev_id: string;
            protocol_version: number;
            app_version: number;
            library_type: number;
            category: string;
            sensor: boolean;
            sleep_capable: boolean;
            listening: boolean;
            wake_up_interval: number;
            S2_keys: string[];
            generic: string;
            specific: string;
            name: string;
            location: string;
            zwave_plus: {
                version: number;
                node_type: number;
                role_type: number;
                installer_icon: number;
                user_icon: number;
            };
            interfaces: {name: string; version: number}[];
        }
    }

    namespace Http {
        interface Client {
            url: string;
            app_key?: string;
            app_type: string;
            app_secret?: string;
            unsafe: boolean;
            logger: Logger;
            api_version: string;
            enableCallback: boolean;
            cert?: string;
            servername?: string;
            callback: (err: Error) => void;
            onError: (err: Error) => void;

            request<T>(method: string, path: string, options?: Options): Promise<Response<T>>;
            request<T>(method: string, path: string, body: any, options?: Options): Promise<Response<T>>;

            post<T>(path: string, body: any, options?: Options): Promise<Response<T>>;
            delete(path: string, options?: Options): Promise<Response<void>>;
            get<T>(path: string, options?: Options): Promise<Response<T>>;
            put<T>(path: string, body: any, options?: Options): Promise<Response<T>>;
            patch<T>(path: string, body: any, options?: Options): Promise<Response<T>>;
        }

        interface Options {
            token?: string;
            basic?: {username: string; password: string};
            app_key?: string;
            unsafe?: boolean;
            api_version?: string;
            app_type?: string;
            whoType?: string;
            who?: string;
            query?: any;
            timeout?: number;
        }

        /** The Error thrown for response status code >= 400. */
        interface ResponseError extends Error {
            statusCode: number;
            statusText: string;
            code: any;
            parameters: any;
            message: any;
            details: any;
            error: any;
            body: any;
        }

        interface Response<T> {
            /** The HTTP status code of the response. */
            statusCode: number;

            /** The response payload. */
            data: T;
        }

        interface ResponseMaybe<T> {
            /** The HTTP status code of the response. */
            statusCode: number;

            /** If `statusCode` is 300..399, the data will be the status text.
             * Otherwise it will be the response payload. */
            data: T | string;
        }
    }
}

export = Sbk;
