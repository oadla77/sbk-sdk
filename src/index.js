var HttpClient = require('./lib/http-client');
var events = require('events');
var inherits = require('inherits');
var StreamAPI = require('./lib/stream_api');
var _ = require('lodash');
var Logger = require('./lib/logger');

function Sbk(options){
    options         = options || {};
    this.base_url   = options.url || "https://api.sbk.com";
    this.apiVersion = options.apiVersion || "1.0";
    this.auth       = options.auth || {};

    this.logger     = new Logger((options.log_level >= 0) ? options.log_level : 6);

    this.http = new HttpClient(this.base_url, {app_key: options.app_key, unsafe: options.unsafe, logger: this.logger, api_version: this.api_version, app_type: options.app_type, cert: options.cert, servername: options.servername });

    // Install all supported API endpoints
    //this.sectors        = require("./lib/methods/sectors")({ platform: this });

    this.events         = require("./lib/methods/events")({ platform: this });
    this.notifications  = require("./lib/methods/notifications")({ platform: this });
    this.user           = require("./lib/methods/user")({ platform: this });
    this.system         = require("./lib/methods/system")({ platform: this });
    this.authentication = require("./lib/methods/auth")({ platform: this });
    this.calls = require("./lib/methods/calls")({ platform: this });
}

inherits(Sbk, events.EventEmitter);

Sbk.prototype.authenticate = require('./lib/methods/authenticate');
Sbk.prototype.register = require('./lib/methods/register');

module.exports = Sbk;
