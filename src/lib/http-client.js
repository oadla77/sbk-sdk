var request = require('request');

var MixinPromise = require('./mixin_promise');
var _ = require('lodash');

function HttpClient(baseUrl, options) {
    options = options || {};
    this.url = baseUrl;
    this.app_key = options.app_key;
    this.app_type = options.app_type || 'browser';
    this.app_secret = options.app_secret;
    this.unsafe = options.unsafe;
    this.logger = options.logger;
    this.api_version = options.api_version;
    this.callback = _.noop; // An optional default callback that can be used on Error to do various actions
    this.onError = _.noop;  // Error callback related to an auth module
    this.enableCallback = options.enableCallback || true;
    this.cert = options.cert;
    this.servername = options.servername;
}

HttpClient.prototype.request = function(method, path, body, options) {
    var _this = this;
    this.logger.trace("Executing HTTP request " + method + " " + path);
    if(body && body.constructor === Object) body = _.omit(body, function(prop){ if(typeof(prop)==='string' && !prop.length) return true });

    if(arguments.length === 3) {
        options = body;
        body = undefined;
    }
    else if(arguments.length === 2) {
        options = {};
    }

    options = options || {};

    return new MixinPromise(function(resolve, reject) {

        var headers = {};

        // Walk around fix: when json:true, 304 cause an issue (as the body is empty!);
        headers['content-type'] = 'application/json';

        if (options.token) {
            _this.logger.trace("Token was provided. Adding Authorization header");
            headers['authorization'] = "Bearer " + options.token;
            _this.logger.trace(headers['authorization']);
        }
        else if (options.basic) {
            _this.logger.trace("Basic credentials were provided. Adding Authorization header");
            var str = new Buffer(options.basic.username + ":" + options.basic.password, "utf8").toString("base64");
            headers['authorization'] = "Basic " + str;
        }

        if (options.app_key || _this.app_key) {
            _this.logger.trace("Configuring X-App-Key to ", options.app_key || _this.app_key);
            headers['x-app-key'] = options.app_key || _this.app_key;
        }

        if (options.unsafe || _this.unsafe) {
            _this.logger.trace("Indicating to the platform that we are in the browser (unsafe)");
            headers['x-unsafe-auth'] = options.unsafe || _this.unsafe;
        }

        if (options.api_version || _this.api_version) {
            headers['x-api-version'] = options.api_version || _this.api_version;
        }

        if (options.app_type || _this.app_type) {
            headers['x-app-type'] = options.app_type || _this.app_type;
        }

        if (options.whoType) {
            headers['x-who-type'] = options.whoType;
        }

        if (options.who) {
            headers['x-who'] = options.who;
        }

        _this.logger.trace("Request URL is: " + method + " " + _this.url + path);

        return new MixinPromise(function (resolve, reject) {
            request({
                method: method,
                url: _this.url + path,
                body: _.isObject(body) ? JSON.stringify(body) : '',
                headers: headers,
                verbose: false,
                //withCredentials: true,
                ca: _this.cert,
                servername: _this.servername,
                timeout: options.timeout || 120000,
                qs: options.query || null
            }, function (err, resp, body) {
                if (err) {
                    _this.logger.error("HTTP ERROR:", err);
                    reject(err);
                }
                else {
                    _this.logger.trace("Receive a valid HTTP response");
                    if (resp.statusCode >= 400) {
                        _this.logger.error("Received status code %d", resp.statusCode);
                        err = new Error("HTTP " + resp.statusCode + ": " + method + " " + path);
                        err.statusCode = resp.statusCode;
                        err.statusText = resp.statusText;

                        if (_isJson(body)) body = JSON.parse(body);
                        err.code = body.code;
                        err.parameters = body.parameters;
                        err.message = body.message;

                        if (body.details) err.details = body.details;

                        err.error = resp.error;
                        err.body = body;
                        reject(err);
                    }
                    else if (resp.statusCode >= 300 && resp.statusCode < 400) {
                        resolve({ statusCode: resp.statusCode,  data: resp.statusText });
                    }
                    else if (resp.statusCode === 0) {
                        _this.logger.error("Unable to connect to platform");
                        err = new Error("Unable to connect to server");
                        err.statusCode = 0;
                        reject(err);
                    }
                    else if (body) {
                        _this.logger.trace("Receive a valid body");
                        if (_.isString(body) && _isJson(body)) {
                            body = JSON.parse(body);
                        }

                        if (body.data) {
                            resolve(body);
                        }
                        else {
                            _this.logger.trace("Resolving response promise. Status Code=", resp.statusCode);
                            resolve({statusCode: resp.statusCode, data: body});
                        }
                    }
                    else {
                        _this.logger.trace("Resolving without body. Status code = %d", resp.statusCode);
                        resolve({statusCode: resp.statusCode});
                    }
                }
            });
        }).then(function (res) {
            resolve(res);
        }).catch(function (error) {
            _this.onError(error);
            if (_this.enableCallback) {
                var p = _this.callback(error);
                var isPromise = _.isObject(p) && _.isFunction(p.then);

                if (isPromise) {
                    p.then(function () {
                        reject(error);
                    });
                }
                else reject(error);
            }
        });
    });
};

HttpClient.prototype.post = function(path, body, options) {
    return this.request('POST', path, body, options);
};

HttpClient.prototype.delete = function(path, options) {
    return this.request('DELETE', path, options);
};

HttpClient.prototype.get = function(path, options) {
    return this.request('GET', path, options);
};

HttpClient.prototype.put = function(path, body, options) {
    if(arguments.length === 2) {
        options = body;
        body = undefined;
    }
    return this.request('PUT', path, body, options);
};

HttpClient.prototype.patch = function(path, body, options) {
    return this.request('PATCH', path, body, options);
};

function _isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

module.exports = HttpClient;
