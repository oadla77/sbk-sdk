'use strict';

var _ = require('lodash');

function Auth(options) {
    options = options || {};
    this.platform = options.platform;
}

Auth.prototype.register = function(user){
    return this.platform.http.post('/auth/register', user);
};

Auth.prototype.gatewayNonce = function(gatewayId){
    return this.platform.http.post('/devices/'+gatewayId+'/auth/nonce', {}, { token: this.platform.auth.token });
};

Auth.prototype.forgot = function(email){
    return this.platform.http.get('/auth/forgot/'+email);
};

Auth.prototype.resendValidateEmail = function(email){
    return this.platform.http.get('/auth/resend/'+email);
};

Auth.prototype.reset = function(resetToken){
    return this.platform.http.get('/auth/reset/'+resetToken);
};

Auth.prototype.resetPassword = function(resetToken, passwordInfo){
    return this.platform.http.post('/auth/reset/'+resetToken, passwordInfo);
};

Auth.prototype.confirm = function(key){
    return this.platform.http.get('/auth/confirm/'+key);
};

// Verify token
Auth.prototype.verify = function(){
    var self = this;
    var token = this.platform.auth.token;
    return this.platform.http.get('/auth/verify/' + token, { token: token })
        .then(function(res){
            _.set(Auth, 'auth.subscribeKey', _.get(res, 'data.subscribeKey'));
            _.set(Auth, 'auth.publishKey', _.get(res, 'data.publishKey'));
            self.platform.emit('auth', res.data);
            return res.data;
        });
};

// Renew token
Auth.prototype.renew = function(){
    var token = this.platform.auth.token;
    return this.platform.http.post('/auth/renew', {}, { token: token })
        .then(function(res){
            _.set(Auth, 'auth.subscribeKey', _.get(res, 'data.subscribeKey'));
            _.set(Auth, 'auth.publishKey', _.get(res, 'data.publishKey'));
            return res.data;
        });
};

// Authenticate service
Auth.prototype.authenticateService = function(name, apiKey){
    var _this = this;
    return this.platform.http.post('/service/auth', {},
    {
        basic: {
            username: name,
            password: apiKey
        }
    }).then(function(res){
        _this.platform.auth.token = res.data.token
        return res
    })
};

module.exports = function(options) {
    options = options || {};
    return new Auth(options);
};

