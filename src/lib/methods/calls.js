'use strict';

function Calls(options) {
    options = options || {};
    this.platform = options.platform;
}

Calls.prototype.makeCall = function(Callname){
    return this.platform.http.get('/Calls/'+Callname, { token: this.platform.auth.token });
};

Calls.prototype.actionOnCall = function(Callname){
    return this.platform.http.get('/Calls/'+Callname, { token: this.platform.auth.token });
};

module.exports = function(options) {
    options = options || {};
    return new Calls(options);
};
