'use strict';

function System(options) {
    options = options || {};
    this.platform = options.platform;
}

System.prototype.info = function(){
    return this.platform.http.get('/system/info');
};

module.exports = function(options) {
    options = options || {};
    return new System(options);
};
