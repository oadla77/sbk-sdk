var P = require("bluebird").noConflict();

P.prototype.mixin = function(cstor) {
    var o = new cstor();
    o.__p = this;

    o.catch = function(fn) {
        this.__p.catch.call(this.__p, fn.bind(this));
        return this;
    };

    o.then = o.ready = function(fn) {
        this.__p.then.call(this.__p, fn.bind(this));
        return this;
    };

    o.finally = function(fn) {
        this.__p.finally.apply(this.__p, fn.bind(this));
        return this;
    };

    return o;
};

module.exports = P;